## Cette activité sur git vous présente une introduction aux notions de branche, de dépôt distant et de conflit.

Pré-requis: [ git: les bases](https://gitlab.com/oscarfrancois/git__basics)
Dans les instructions suivantes, on suppose que vous avez déjà initialisé un dépôt git et y avez soumis au moins un commit.

Cette activité est une adaptation d'une documentation initialement réalisée par Me Travnjak.


### Les branches et tags

Sans peut-être le savoir, dans l'activité sur les bases de git (cf. pré-requis), vous avez systématiquement travaillé sur une branche.
Que ce soit l'espace de travail ("working directory"), la zone de pré-commit ("staging area") ou la zone de dépôt ("repository"),
ils sont tous à un moment donné associés à une branche donnée.

Par défaut, git vous positionne sur la branche dite maître ("master").

Constatez le via la commande:
```
git status
```

La branche active est en fait stockée simplement dans le fichier: .git/HEAD
Ce fichier contient le chemin vers la branche locale active.
Dans notre cas: refs/heads/master


#### Utilité des branches

Les branches servent communément à travailler en parallèle dans différents contextes.
Par exemple, on peut avoir publié à un client une version V1 de notre logiciel et travailler actuellement sur une version future V2.
La version V1 étant en production, de la maintenance est à prévoir.
On va donc suivant l'activité que l'on souhaite réaliser utiliser alternativement la branche V1 (pour par exemple des correctifs) et 
la branche V2.

Voici un exemple concret:
Les universités suisses utilisent en interne le système de fichier distribué suivant: 
```
http://drive.switch.ch
```

Celui-ci est basé sur la solution Nextcloud dont le code source serveur est en libre accès à l'adresse suivante: 
```
https://github.com/nextcloud/server
```

On constate la présence d'une branche pour chaque version majeure (branche au format: stableXY)
On constate aussi des branches avec des préfixes tels que: enh / fix / feature
C'est un autre usage possible des branches qui peuvent être créées à l'occasion:
- de correctifs ("fix")
- d'améliorations ("enhancement")
- de nouvelles fonctionnalités ("features")

Vous pouvez aussi constater une seconde catégorie dénommée "Tags" à côté de "Branches".

#### Notion de tag 

Un tag est fondamentalement différent d'une branche dans le sens où un tag est en quelque sorte un "tampon" 
apposé sur une branche à un instant T. Un tag fera donc référence à un seul commit contrairement aux branches 
qui contiennent une successions de commits.

Un tag sert généralement à garder une trace précise d'un livrable fourni officiellement aux utilisateurs.

Pour illustrer cette notion de tag, nous pouvons prendre connaissance des tags du projet Nextcloud:
```
https://github.com/nextcloud/server
```
Remarque: outre les numéros de version, vous constaterez aussi des suffixes de type: betaXY ou bien RCXY.
Cela correspond à des versions généralement livrées à un panel d'utilisateurs de confiance (ex: employés de la société).


### Les dépôts distants

Dans un système distribué tel que git, on définit souvent un des noeuds distants comme un point de référence du projet.
Celui-ci est souvent constitué d'un serveur ayant seulement vocation à recevoir les commits des développeurs 
et à déclencher différents actions (ex: déclenchement des tests automatisés, génération de la webapp, etc).

Afin de travailler efficacement et de pouvoir soumettre ses changements vers le serveur, on génère tout d'abord un jeu de clefs de sécurité
qui est ajouté au serveur.
Ces étapes sont décrites dans l'activité suivante:
![clonage d'un dépôt distant](https://gitlab.com/oscarfrancois/git__clone)



### Les conflits

Tout d'abord une illustration humoristique sur une façon un peu cavalière d'éviter les conflits:
![éviter les conflits]( how_to_avoid_merge_conflict.jpg)

En effet, la référence officielle étant le serveur distant, le premier à y avoir envoyé ses changements deviendra la référence!
En cas de changements contradictoire entre 2 développeurs ayant par exemple édité le même fichier, 
c'est donc celui qui publiera ses changements en dernier qui devra résoudre le conflit.

Plus sérieusement, la première recommandation pour résoudre les conflits est de les éviter en appliquant de bonnes pratiques:
- lorsque vous travaillez dans un même espace de travail ("open-space"), 
discutez entre vous pour ne pas travailler sur le même fichier en même temps.
- si vous devez impérativement travailler sur le même fichier, convenez de zones à part 
- pour que les changements puissent être fusionnés automatiquement sans conflits.
- faites régulièrement des "git pull" pour garder votre copie locale à jour.
- publiez régulièrement vos changements sur le serveur distant (plusieurs fois par jour en général et systèmatiquement pour chaque story).

Si vous vous retrouvez tout de même dans un cas de conflit, voici le processus à suivre:

![git_fetch_rebase](git_rebase.png)